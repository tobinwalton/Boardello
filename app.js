var createError    = require('http-errors');
var express        = require('express');
var path           = require('path');
var cookieParser   = require('cookie-parser');
var logger         = require('morgan');
var sassMiddleware = require('node-sass-middleware');
var boards         = require('./boardSettings'); // Experimental boards object
var indexRouter    = require('./routes/index');
var postRouter     = require('./routes/post');
var threadRouter   = require('./routes/thread');
var mongoose       = require('mongoose');
var router         = express.Router();

// Development database connection
var mongoDB        = 'mongodb://boardello_dev:asd123@ds235768.mlab.com:35768/boardello';
mongoose.connect(mongoDB);
mongoose.Promise   = global.Promise;
var db             = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection Error'));

// Models
var postModel      = require('./models/post');
var threadModel    = require('./models/thread');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// Generate routes for all boards configured in boardSettings.js
function createBoardRoutes(boards) {
  boards.forEach(board => {
    /* 
      These routes are dynamically generated for each board in boardSettings.js
    */
    app.get(('/' + board.name), function (req, res, next) {
      db.collection('threadmodels').find({ 'board': board.name }).sort([['updated', -1]]).toArray(function (err, results) {
        if (err) return console.log(err);
        res.render(board.name, { title: board.title, threads: results, boards: boards });
      });
    });

    app.post(('/createpost/:threadId'), function (req, res, next) {
      var threadId = req.params.threadId;
      var name = '';
      if (req.body.name_field === '') {
        name = 'Anonymous';
      } else {
        name = req.body.name_field;
      }
      var post = new postModel({
        board: board.name,
        thread: threadId,
        name: name,
        comment: req.body.comment_field
      });

      // When a comment is made, bump that comment's thread to the top of the thread list
      threadModel.findOneAndUpdate({_id: threadId}, {$set: {updated: Date.now()}}, function (err, doc) {
        if (err) return console.log(err);
      });

      post.save(function (err) {
        if (err) return handleError(err);
        res.redirect(`/thread/${threadId}`);
      });
    });

    app.post(('/' + board.name + '/create_thread'), function (req, res, next) {
      var name = '';
      if (req.body.name_field === '') {
        name = 'Anonymous';
      } else {
        name = req.body.name_field;
      }

      var thread = new threadModel({
        board: board.name,
        name: name,
        title: req.body.title_field,
        comment: req.body.comment_field
      });

      thread.save(function (err) {
        if (err) return handleError(err);
        res.redirect(`/${board.name}`);
      });
    });

  });
};

createBoardRoutes(boards);

app.use('/', indexRouter);
app.use('/', postRouter);
app.use('/', threadRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
