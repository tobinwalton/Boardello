var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var threadModelSchema = new Schema({
  _threadId: Schema.Types.ObjectId,
  board: String,
  name: { 
    type: String,
    maxlength: 25,
    default: 'Anonymous'
  },
  updated: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    maxlength: 256
  },
  comment: {
    type: String,
    maxlength: 25000
  }
});

var threadModel = mongoose.model('threadModel', threadModelSchema);

module.exports = threadModel;