extends layout 
block content
  h1 ${board.title}
  h3 ${board.tagline}

  form(action='/${board.name}/create_post', method='post')
    label(for='name') Name (leave empty for anonymous)
    br
    input(id='name' type='text' name='name_field' value='user name')
    br
    textarea(id='comment' name='comment_field' rows='20' cols='120')
    br
    input(type='submit' value='Post')
    `,