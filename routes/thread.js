var express        = require('express');
var thread         = require('../models/thread');
var mongoose       = require('mongoose');
var boardSettings  = require('../boardSettings');
var router         = express.Router();
var ObjectId       = mongoose.Types.ObjectId;

var mongoDB       = 'mongodb://boardello_dev:asd123@ds235768.mlab.com:35768/boardello';
mongoose.connect(mongoDB);
mongoose.Promise  = global.Promise;
var db            = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection Error'));

router.get('/threads', function (req, res, next) {
  res.send('all threads route');
});

router.get('/thread/:threadId', function (req, res, next) {
  db.collection('threadmodels').find({ '_id': ObjectId(req.params.threadId) }).toArray(function (err, threads) {
    if (err) return console.log(err);
    db.collection('postmodels').find({ 'thread': req.params.threadId }).toArray(function (err, posts) {
        if (err) return console.log(err);
        res.render('thread', { title: "Thread", thread: threads[0], posts: posts, boards: boardSettings });
    });
    // res.render('thread', { title: "Thread", thread: result[0] });
  });
  // var query = thread.find({})
});

module.exports = router;