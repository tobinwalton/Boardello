var express        = require('express');
var boardSettings  = require('../boardSettings');
var mongoose       = require('mongoose');
// var posts          = require('../models/post');
var router         = express.Router();

var mongoDB        = 'mongodb://boardello_dev:asd123@ds235768.mlab.com:35768/boardello';
mongoose.connect(mongoDB);
mongoose.Promise   = global.Promise;
var db             = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection Error'));


router.get('/', function(req, res, next) {

  db.collection('postmodels').find({}).sort([['date', -1]]).limit(20).toArray(function (err, posts) {
    if (err) return console.log(err);
      res.render('index', { title: 'Boardello', boards: boardSettings, posts: posts });
  });
});

module.exports = router;
